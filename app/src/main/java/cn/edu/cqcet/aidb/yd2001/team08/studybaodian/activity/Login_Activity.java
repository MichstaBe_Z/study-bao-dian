package cn.edu.cqcet.aidb.yd2001.team08.studybaodian.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.MainActivity;
import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.R;
import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.utils.MD5Utils;

public class Login_Activity extends AppCompatActivity {
    private EditText loginName,loginPsw;
    private Button login;
    private TextView forget,register;
    private ImageView back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();

    }
    public void init(){
        login=findViewById(R.id.btn_login);
        loginName=findViewById(R.id.et_loginName);
        back=findViewById(R.id.img_back1);
        forget=findViewById(R.id.tx_forget);
        register=findViewById(R.id.tx_register);
        loginPsw=findViewById(R.id.et_loginPsw);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Login_Activity.this.finish();
            }
        });
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Login_Activity.this,RegisterActivity.class);
                startActivity(intent);
            }
        });
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String userName=loginName.getText().toString().trim();
                String passWord=loginPsw.getText().toString().trim();
                String md5Psw= MD5Utils.md5(passWord);
                String spPsw=readPsw(userName);
                if(TextUtils.isEmpty(userName)){
                    Toast.makeText(Login_Activity.this,"请输入用户名",Toast.LENGTH_LONG).show();
                    return;
                }
                else if(TextUtils.isEmpty(passWord)){
                    Toast.makeText(Login_Activity.this,"请输入密码",Toast.LENGTH_LONG).show();
                    return;
                }
                else if(md5Psw.equals(spPsw)){
                    Toast.makeText(Login_Activity.this,"登录成功",Toast.LENGTH_LONG).show();
                    saveLoginStatus(true,userName);
                    Intent intent=new Intent();
                    intent.putExtra("isLogin",true);
                    setResult(RESULT_OK,intent);
                    intent.setClass(Login_Activity.this, HomeActivity.class);
                    startActivity(intent);
                    return;
                }
                else if ((!TextUtils.isEmpty(spPsw)&&!md5Psw.equals(spPsw))){
                    Toast.makeText(Login_Activity.this,"用户名或密码错误",Toast.LENGTH_LONG).show();
                    return;
                }else {
                    Toast.makeText(Login_Activity.this,"用户不存在",Toast.LENGTH_LONG).show();
                    return;
                }
            }
        });
    }
    private String readPsw(String userName){
        SharedPreferences sp=getSharedPreferences("loginInfo",MODE_PRIVATE);
        return sp.getString(userName,"");
    }
    private void saveLoginStatus(boolean status,String userName){
        SharedPreferences sp=getSharedPreferences("loginInfo",MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();
        editor.putBoolean("isLogin",status);
        editor.putString("LoginUserName",userName);
        editor.commit();
    }
    protected void onActivityResult(int requestCode,int resultCode,Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (intent!=null){
            String userName=intent.getStringExtra("userName");
            if (!TextUtils.isEmpty(userName)) {
                loginName.setText(userName);
                loginName.setSelection(userName.length());
            }
        }
    }
}