package cn.edu.cqcet.aidb.yd2001.team08.studybaodian.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.R;
import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.utils.MD5Utils;

public class RegisterActivity extends AppCompatActivity {
    private EditText name,et_psw,et_again_psw;
    private ImageView img_back,img_head;
    private Button btn_reg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        init();
    }
    public void init(){
        name=findViewById(R.id.et_name);
        et_psw=findViewById(R.id.et_password);
        et_again_psw=findViewById(R.id.et_again_password);
        img_back=findViewById(R.id.img_back);
        img_head=findViewById(R.id.img_headImg);
        btn_reg=findViewById(R.id.btn_register);
        img_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setClass(RegisterActivity.this,Login_Activity.class);
                startActivity(intent);
            }
        });
        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username=name.getText().toString();
                String psw=et_psw.getText().toString();
                String again_psw=et_again_psw.getText().toString();
                if (TextUtils.isEmpty(username)){
                    Toast.makeText(RegisterActivity.this,"请输入账号",Toast.LENGTH_LONG).show();
                    return;
                }
                else if (TextUtils.isEmpty(psw)){
                    Toast.makeText(RegisterActivity.this,"请输入密码",Toast.LENGTH_LONG).show();
                    return;
                }
                else if (TextUtils.isEmpty(again_psw)){
                    Toast.makeText(RegisterActivity.this,"请再次输入密码",Toast.LENGTH_LONG).show();
                    return;
                }
                else if(!psw.equals(again_psw)){
                    Toast.makeText(RegisterActivity.this,"两次输入密码不一致",Toast.LENGTH_LONG).show();
                    return;
                }
                else if (isExistUserName(username)){
                    Toast.makeText(RegisterActivity.this,"用户名已存在",Toast.LENGTH_LONG).show();
                    return;
                }
                else {
                    Toast.makeText(RegisterActivity.this,"注册成功",Toast.LENGTH_LONG).show();
                    saveRegisterInfo(username,psw);
                    Intent intent=new Intent();
                    intent.putExtra("userName",username);
                    setResult(RESULT_OK,intent);
                    intent.setClass(RegisterActivity.this,Login_Activity.class);
                    startActivity(intent);
                    return;
                }
            }
        });
    }

    public boolean isExistUserName(String username){
            boolean has_userName=false;
        SharedPreferences sp=getSharedPreferences("loginInfo",MODE_PRIVATE);
        String spPsw=sp.getString(username,"");
        if (!TextUtils.isEmpty(spPsw)){
            has_userName=true;
        }
        return has_userName;
    }
    public void saveRegisterInfo(String userName,String psw){
        String md5Psw= MD5Utils.md5(psw);
        SharedPreferences sp=getSharedPreferences("loginInfo",MODE_PRIVATE);
        SharedPreferences.Editor editor=sp.edit();
        editor.putString(userName,md5Psw);
        editor.commit();
    }
}