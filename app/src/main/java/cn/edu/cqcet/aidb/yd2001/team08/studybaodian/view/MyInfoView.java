package cn.edu.cqcet.aidb.yd2001.team08.studybaodian.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.R;
import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.activity.Login_Activity;
import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.utils.Analysisutils;

/**
 * @author zhulinxi
 * @contact: 3147066533@qq.com
 * @group: Team08
 * @date :2022/6/22 9:19
 */
public class MyInfoView {
    public ImageView iv_head_icon;
    private LinearLayout ll_head;
    private RelativeLayout rl_course_history,rl_setting;
    private TextView tv_user_name;
    private Activity mContext;
    private LayoutInflater mInflater;
    private View mCurrentView;
    public MyInfoView(Activity context){
        mContext=context;
        mInflater=LayoutInflater.from(mContext);
    }
    private void createView(){
        initView();
    }
    private void initView(){
        mCurrentView=mInflater.inflate(R.layout.main_view_myinfo,null);
        ll_head=mCurrentView.findViewById(R.id.ll_head);
        rl_course_history=mCurrentView.findViewById(R.id.rl_course_history);
        rl_setting=mCurrentView.findViewById(R.id.rl_setting);
        tv_user_name=mCurrentView.findViewById(R.id.tv_user_name);
        mCurrentView.setVisibility(View.VISIBLE);
        setLoginParams(readLoginStatus());
        ll_head.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (readLoginStatus()){
                    //已登录跳转到个人资料界面
                }else {
                    //未登录跳转到登录界面
                }
                Intent intent=new Intent(mContext, Login_Activity.class);
                mContext.startActivityForResult(intent,1);
            }
        });
        rl_course_history.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (readLoginStatus()){
                    //跳转到播放记录界面
                }else {
                    Toast.makeText(mContext,"您还未登录，请先登录",Toast.LENGTH_SHORT).show();
                }
                Intent intent=new Intent(mContext, Login_Activity.class);
                mContext.startActivityForResult(intent,1);
            }
        });
        rl_setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (readLoginStatus()){
                    //跳转到设置界面
                }else {
                    Toast.makeText(mContext,"您还未登录，请先登录",Toast.LENGTH_SHORT).show();
                }
                Intent intent=new Intent(mContext, Login_Activity.class);
                mContext.startActivityForResult(intent,1);
            }
        });
    }
    public void setLoginParams(boolean isLogin){
        if(isLogin) {
            tv_user_name.setText(Analysisutils.readLoginUserName(mContext));
        }
        else {
            tv_user_name.setText("点击登录");
        }
    }
    public View getView(){
        if(mCurrentView==null){
            createView();
        }
        return mCurrentView;
    }
    public void showView(){
        if(mCurrentView==null){
            createView();
        }
        mCurrentView.setVisibility(View.VISIBLE);
    }
    private boolean readLoginStatus(){
        SharedPreferences sp=mContext.getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        boolean isLogin=sp.getBoolean("isLogin",false);
        return isLogin;
    }
}
