package cn.edu.cqcet.aidb.yd2001.team08.studybaodian.activity;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.R;
import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.view.MyInfoView;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener{
    private MyInfoView mMyInfoView;
    private View mian_title_bar,mCourseBtn,mExercisesBtn,mMyinfoBtn;
    private FrameLayout mBodyLayout;
    private LinearLayout mBottonLayout;
    private TextView title_bar,tv_course,tv_exercises,tv_myinfo,tv_mian_title;
    private ImageView iv_course,iv_exercises,iv_myinfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
        initBottomLayout();
        handleListener();
        clearSelectStatus();
    }

    private void handleListener() {
        for(int i=0;i<mBottonLayout.getChildCount();i++){
            mBottonLayout.getChildAt(i).setOnClickListener(this);
        }
    }

    private void clearSelectStatus() {
        removeAllView();//遍历主视图当中的bodyLayout所有组件
        clearButtomImageState();//清楚底部导航栏中的状态

    }

    private void setSlectedStatus(int index) {
        switch(index){
            case 0://课程
                clearButtomImageState();
                tv_course.setTextColor(Color.parseColor("#0C92FF"));
                iv_course.setImageResource(R.drawable.main_course_icon_selected);
                tv_mian_title.setText(R.string.COURSE_TITLle);
                mian_title_bar.setVisibility(View.VISIBLE);
                creatView(0);//加载课程界面
                break;
            case 1://加载习题界面
                clearButtomImageState();
                tv_exercises.setTextColor(Color.parseColor("#0C92FF"));
                tv_mian_title.setText(R.string.EXERCISES_TITLE);
                iv_exercises.setImageResource(R.drawable.main_exercises_icon_selected);
                creatView(2);
                mian_title_bar.setVisibility(View.VISIBLE);
                break;
            case 2://加载我的界面

                clearButtomImageState();
                tv_myinfo.setTextColor(Color.parseColor("#0C92FF"));
                iv_myinfo.setImageResource(R.drawable.main_my_icon_selected);
                mian_title_bar.setVisibility(View.INVISIBLE);
                creatView(2);
                break;
        }
    }

    private void creatView(int viewIndex) {//根据选择的创建body视图界面
        switch (viewIndex){
            case 0:
                // 课程界面
                break;
            case 1:
                // 习题界面
                break;
            case 2:
                // 我的界面
                if(mMyInfoView==null){
                    mMyInfoView=new MyInfoView(this);
                    mBodyLayout.addView(mMyInfoView.getView());
                }else {
                    mMyInfoView.getView();
                }
                mMyInfoView.showView();
                break;
        }
    }

    private void clearButtomImageState() {//清除图片和文字状态和选中
        iv_course.setSelected(false);
        iv_exercises.setSelected(false);
        iv_myinfo.setSelected(false);
        tv_course.setTextColor(Color.parseColor("#666666"));
        tv_exercises.setTextColor(Color.parseColor("#666666"));
        tv_myinfo.setTextColor(Color.parseColor("#666666"));
        iv_course.setImageResource(R.drawable.main_course_icon);
        iv_exercises.setImageResource(R.drawable.main_exercises_icon);
        iv_myinfo.setImageResource(R.drawable.main_my_icon);
    }

    private void removeAllView() {
        for(int i=0;i<mBodyLayout.getChildCount();i++){
            mBodyLayout.getChildAt(i).setVisibility(View.INVISIBLE);
        }
    }

    private void initBottomLayout() {
        mBottonLayout=findViewById(R.id.main_bottom_bar);
        mCourseBtn=findViewById(R.id.bottom_bar_course_btn);
        mExercisesBtn=findViewById(R.id.bottom_bar_exercises_btn);
        mMyinfoBtn=findViewById(R.id.bottom_bar_myinfo_btn);
        tv_course=findViewById(R.id.bottom_bar_course_text);
        tv_exercises=findViewById(R.id.bottom_bar_exercises_text);
        tv_myinfo=findViewById(R.id.bottom_bar_myinfo_text);
        iv_course=findViewById(R.id.bottom_bar_course_img);
        iv_exercises=findViewById(R.id.bottom_bar_exercises_img);
        iv_myinfo=findViewById(R.id.bottom_bar_myinfo_img);
    }

    private void initBodyLayout() {
        mBodyLayout=findViewById(R.id.mian_body);
    }

    private void init() {
        tv_mian_title=findViewById(R.id.mian_title);
        mian_title_bar=findViewById(R.id.main_title_bar);
        title_bar=findViewById(R.id.mian_title);
        tv_course=findViewById(R.id.bottom_bar_course_text);
        tv_exercises=findViewById(R.id.bottom_bar_exercises_text);
        tv_myinfo=findViewById(R.id.bottom_bar_myinfo_text);
        initBodyLayout();
        tv_mian_title.setText("首页");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bottom_bar_course_btn:
                clearSelectStatus();
                setSlectedStatus(0);
                break;
            case R.id.bottom_bar_exercises_btn:
                clearSelectStatus();
                setSlectedStatus(1);
                break;
            case R.id.bottom_bar_myinfo_btn:
                clearSelectStatus();
                setSlectedStatus(2);
                break;
        }
    }
    protected long exitTime;
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode==KeyEvent.KEYCODE_BACK && event.getAction()==KeyEvent.ACTION_DOWN){
            if ((System.currentTimeMillis()-exitTime)>2000){
                Toast.makeText(HomeActivity.this,"再按一次退出学习宝典",Toast.LENGTH_SHORT).show();
                exitTime=System.currentTimeMillis();
            }
            else{HomeActivity.this.finish();
                if (readLoginStatus()){
                    clearLoginStatus();
                }
                System.exit(0);
            }return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void clearLoginStatus() {
        SharedPreferences sharedPreferences = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor editot = sharedPreferences.edit();
        editot.putBoolean("isLogin",false);
        editot.putString("loginUserName","");
        editot.commit();
    }

    private boolean readLoginStatus() {
        SharedPreferences sharedPreferences = getSharedPreferences("loginInfo", Context.MODE_PRIVATE);
        boolean isLogin=sharedPreferences.getBoolean("isLogin",false);
        return isLogin;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (intent!=null){
            boolean isLogin=intent.getBooleanExtra("isLogin",false);
            if (isLogin) {
                clearButtomImageState();
                setSlectedStatus(0);
            }
            if (mMyInfoView!=null){
                mMyInfoView.setLoginParams(isLogin);
            }
        }
    }

}

