package cn.edu.cqcet.aidb.yd2001.team08.studybaodian.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.R;

/**
 * @author zhulinxi
 * @contact: 3147066533@qq.com
 * @group: Team08
 * @date :2022/6/21 9:02
 */
public class ViewPagerAdapter extends RecyclerView.Adapter<ViewPagerAdapter.ViewHolder> {
    private List<Integer> colors;
    public ViewPagerAdapter(List<Integer> colors){
        this.colors = colors;
    }
    @NonNull
    @Override
    public ViewPagerAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.main_adbannner2_item,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewPagerAdapter.ViewHolder holder, int position) {
        int i = position % 4;
        holder.titleTv.setText("");
        holder.container.setBackgroundResource(colors.get(i));
    }

    @Override
    public int getItemCount() {
        return Integer.MAX_VALUE;
    }
    class ViewHolder extends RecyclerView.ViewHolder{
        RelativeLayout container;
        TextView titleTv;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            titleTv = itemView.findViewById(R.id.tv_title);
        }
    }
}
