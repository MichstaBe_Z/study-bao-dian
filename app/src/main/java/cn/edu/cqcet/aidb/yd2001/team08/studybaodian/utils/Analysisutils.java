package cn.edu.cqcet.aidb.yd2001.team08.studybaodian.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * @author zhulinxi
 * @contact: 3147066533@qq.com
 * @group: Team08
 * @date :2022/6/22 9:16
 */
public class Analysisutils {
    public static String readLoginUserName(Context context){
        SharedPreferences sp=context.getSharedPreferences("loginInfo",Context.MODE_PRIVATE);
        String userName=sp.getString("loginUserName","");
        return userName;
    }
}
