package cn.edu.cqcet.aidb.yd2001.team08.studybaodian;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.activity.HomeActivity;
import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.activity.RegisterActivity;

public class MainActivity extends AppCompatActivity {
    Handler mHandler=new Handler();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent=new Intent();
                intent.setClass(MainActivity.this, RegisterActivity.class);
                startActivity(intent);
                finish();//销毁欢迎页面
            }
        },3000);//3秒后跳转
    }
}