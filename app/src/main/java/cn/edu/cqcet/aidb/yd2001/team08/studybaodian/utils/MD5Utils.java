package cn.edu.cqcet.aidb.yd2001.team08.studybaodian.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author zhulinxi
 * @contact: 3147066533@qq.com
 * @group: Team08
 * @date :2022/6/20 17:02
 */
public class MD5Utils {
    public static String md5(String text){
        MessageDigest digest=null;
        try {
            digest=MessageDigest.getInstance("md5");
            byte[] result=digest.digest(text.getBytes());
            StringBuilder sb=new StringBuilder();
            for (byte b:result){
                int number=b&0xff;
                String hex=Integer.toHexString(number);
                if (hex.length()==1){
                    sb.append("0"+hex);
                }else {
                    sb.append(hex);
                }
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return "";
        }

    }
}
