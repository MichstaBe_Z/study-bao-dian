package cn.edu.cqcet.aidb.yd2001.team08.studybaodian.bean;

/**
 * @author zhulinxi
 * @contact: 3147066533@qq.com
 * @group: Team08
 * @date :2022/6/21 10:21
 */
public class CourseBean {
    public int id;
    public String imgTitle;
    public String title;
    public String intro;
    public String icon;
}
