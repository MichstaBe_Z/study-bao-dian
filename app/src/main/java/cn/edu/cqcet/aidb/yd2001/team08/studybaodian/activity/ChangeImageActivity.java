package cn.edu.cqcet.aidb.yd2001.team08.studybaodian.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.R;
import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.adapter.ViewPagerAdapter;


public class ChangeImageActivity extends AppCompatActivity {
    private ViewPager2 viewPager2;
    private int lastPosition;                           //记录轮播图最后所在的位置
    private List<Integer> colors = new ArrayList<>();   //轮播图的颜色
    private LinearLayout indicatorContainer;            //填充指示点的容器
    private Handler mHandler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_view_course);
        setViewPage();

    }
    public void setViewPage(){
        viewPager2 = findViewById(R.id.viewpager2);
        indicatorContainer = findViewById(R.id.container_indicator);

        initColors();
        //初始化指示点
        initIndicatorDots();

        //添加适配器
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(colors);
        viewPager2.setAdapter(viewPagerAdapter);
        //设置轮播图初始位置在500,以保证可以手动前翻
        viewPager2.setCurrentItem(500);
        //最后所在的位置设置为500
        lastPosition = 500;

        //注册轮播图的滚动事件监听器
        viewPager2.registerOnPageChangeCallback(new ViewPager2.OnPageChangeCallback() {

            @SuppressLint("ResourceType")
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                //轮播时，改变指示点
                int current = position % 4;
                int last = lastPosition % 4;
                indicatorContainer.getChildAt(current).setBackgroundResource(R.xml.shape_dot_selected);
                indicatorContainer.getChildAt(last).setBackgroundResource(R.xml.shape_dot);
                lastPosition = position;
            }
        });
    }

    private void initColors(){
        colors.add(R.drawable.banner_1);
        colors.add(R.drawable.banner_2);
        colors.add(R.drawable.banner_3);
        colors.add(R.drawable.background);
    }

    /**
     * 初始化指示点
     */
    @SuppressLint("ResourceType")
    private void initIndicatorDots(){
        for(int i = 0; i < colors.size(); i++){
            ImageView imageView = new ImageView(this);
            if (i == 0) imageView.setBackgroundResource(R.xml.shape_dot_selected);
            else imageView.setBackgroundResource(R.xml.shape_dot);
            //为指示点添加间距
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMarginEnd(4);
            imageView.setLayoutParams(layoutParams);
            //将指示点添加进容器
            indicatorContainer.addView(imageView);
        }
    }

    /* 当应用被唤醒时，让轮播图开始轮播 */
    @Override
    protected void onResume() {
        super.onResume();
        mHandler.postDelayed(runnable,3000);
    }

    /* 当应用被暂停时，让轮播图停止轮播 */
    @Override
    protected void onPause() {
        super.onPause();
        mHandler.removeCallbacks(runnable);
    }

    private final Runnable runnable = new Runnable() {
        @Override
        public void run() {
            //获得轮播图当前的位置
            int currentPosition = viewPager2.getCurrentItem();
            currentPosition++;
            viewPager2.setCurrentItem(currentPosition,true);
            mHandler.postDelayed(runnable,5000);
        }
    };
}