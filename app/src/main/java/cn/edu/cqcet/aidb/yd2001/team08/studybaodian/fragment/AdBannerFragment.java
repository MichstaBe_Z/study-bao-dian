package cn.edu.cqcet.aidb.yd2001.team08.studybaodian.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import cn.edu.cqcet.aidb.yd2001.team08.studybaodian.R;

/**
 * @author zhulinxi
 * @contact: 3147066533@qq.com
 * @group: Team08
 * @date :2022/6/21 10:23
 */
public class AdBannerFragment extends Fragment {
    private String ab;//广告
    private ImageView iv;//图片
    public static AdBannerFragment newInstance(Bundle args){
        AdBannerFragment af=new AdBannerFragment();
        af.setArguments(args);
        return af;
    }

    public void OnCreate(Bundle saveInstanceState){
        super.onCreate(saveInstanceState);
        Bundle arg=getArguments();
        ab=arg.getString("ab");
    }
    public void onActivityCreated(Bundle saveInstanceState){
        super.onActivityCreated(saveInstanceState);
    }
    public void onResume(){
        super.onResume();
        if (ab!=null){
            if ("banner_1".equals(ab)){
                iv.setImageResource(R.drawable.banner_1);
            }else if("banner_2".equals(ab)) {
                iv.setImageResource(R.drawable.banner_2);
            }
            else if ("banner_3".equals(ab)) {
                iv.setImageResource(R.drawable.banner_3);
            }
        }
    }
    public void onDestroy(){
        super.onDestroy();
        if (iv!=null){
            iv.setImageDrawable(null);
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle saveInstanceState){
        iv=new ImageView(getActivity());
        ViewGroup.LayoutParams lp=new ViewGroup.LayoutParams(ViewGroup.LayoutParams.FILL_PARENT,ViewGroup.LayoutParams.FILL_PARENT);
        iv.setLayoutParams(lp);
        iv.setScaleType(ImageView.ScaleType.FIT_XY);
        return iv;
    }
}
